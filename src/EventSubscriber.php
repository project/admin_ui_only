<?php

namespace Drupal\admin_ui_only;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Prevents html access to non admin pages.
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * A list of routes that should be admin routes.
   */
  private const ADMIN_ROUTES = [
    'entity.user.cancel_form',
    'entity.user.canonical',
    'entity.user.contact_form',
    'entity.user.edit_form',
    'media.oembed_iframe',
    'system.batch_page.html',
    'user.edit',
    'user.login',
    'user.logout',
    'user.logout.confirm',
    'user.page',
    'user.cancel_confirm',
    'user.reset.login',
    'user.reset',
    'user.reset.form',
    'user.well-known.change_password',
    'user.register',
  ];

  public function __construct(protected ConfigFactoryInterface $configFactory, protected RouteBuilderInterface $routeBuilder) {
  }

  /**
   * Triggers a 404 for applicable routes on response.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onKernelRespond(ResponseEvent $event): void {
    if ($event->isMainRequest()) {
      $response = $event->getResponse();
      if (
        $response->getStatusCode() === 200 &&
        // Checking the content type allows JSON responses through.
        str_contains($response->headers->get('Content-Type') ?? '', 'text/html') &&
        $this->deny($event->getRequest())
      ) {
        $route_name = $event->getRequest()->attributes->get(RouteObjectInterface::ROUTE_NAME);
        $message = sprintf("The route '%s' is not an admin route and is unavailable via the frontend", $route_name);
        if ($this->configFactory->get('admin_ui_only.settings')->get('error_code') === 404) {
          throw new NotFoundHttpException($message);
        }
        else {
          throw new AccessDeniedHttpException($message);
        }
      }
    }
  }

  /**
   * Determines if the request should be denied.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return bool
   *   TRUE if the request should be denied and FALSE if should be allowed.
   */
  private function deny(Request $request): bool {
    if ($request->getRequestFormat() !== 'html') {
      return FALSE;
    }
    /** @var \Symfony\Component\Routing\Route|null $route_object */
    $route_object = $request->attributes->get(RouteObjectInterface::ROUTE_OBJECT);
    if ($route_object && $route_object->getOption('_admin_route')) {
      // We are not interested in admin routes.
      return FALSE;
    }
    // Give access to front page. The front page should be set to user/login for
    // the best UX.
    return $request->getPathInfo() !== '/';
  }

  /**
   * Alters existing routes for a specific collection.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The route build event.
   */
  public function onAlterRoutes(RouteBuildEvent $event): void {
    $collection = $event->getRouteCollection();
    foreach (self::ADMIN_ROUTES as $route_name) {
      $route = $collection->get($route_name);
      $route?->setOption('_admin_route', TRUE);
    }
    foreach ($this->configFactory->get('admin_ui_only.settings')->get('routes') as $route_name) {
      $route = $collection->get($route_name);
      $route?->setOption('_admin_route', TRUE);
    }
  }

  /**
   * Rebuilds routes if routes config has changed.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The configuration event.
   */
  public function onConfigSave(ConfigCrudEvent $event): void {
    if ($event->isChanged('routes')) {
      $this->routeBuilder->setRebuildNeeded();
    }
    if ($event->isChanged('error_code')) {
      Cache::invalidateTags(['4xx-response']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['onKernelRespond'];
    $events[RoutingEvents::ALTER][] = ['onAlterRoutes', 30];
    $events[ConfigEvents::SAVE][] = ['onConfigSave'];
    return $events;
  }

}
