<?php

namespace Drupal\admin_ui_only\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Configures the Admin UI Only module.
 */
class SettingsForm extends ConfigFormBase {

  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, protected RouteProviderInterface $routeProvider) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('router.route_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'admin_ui_only.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'admin_ui_only_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('admin_ui_only.settings');

    $form['error_code'] = [
      '#type' => 'radios',
      '#title' => $this->t('Error code'),
      '#default_value' => $config->get('error_code'),
      '#options' => [
        '403' => $this->t('Access denied (403)'),
        '404' => $this->t('Not found (404)'),
      ],
      '#description' => $this->t('Select the error code to use when rejecting a frontend HTML route. 403s have more information about why and 404s disclose less about your site.'),
    ];

    $routes = $config->get('routes');
    $form['routes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Admin routes'),
      '#default_value' => implode("\n", $routes),
      '#rows' => min(20, count($routes) + 3),
      '#description' => $this->t('Add routes to this list to ensure they are accessible if they return HTML. One route name per line.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('admin_ui_only.settings')
      ->set('routes', self::getRoutes($form_state))
      ->set('error_code', $form_state->getValue('error_code'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $missing_routes = [];
    foreach (self::getRoutes($form_state) as $route_name) {
      try {
        $this->routeProvider->getRouteByName($route_name);
      }
      catch (RouteNotFoundException $e) {
        $missing_routes[] = $route_name;
      }
    }
    if (!empty($missing_routes)) {
      $form_state->setErrorByName('routes', $this->t('The following route(s) do not exist: %routes', ['%routes' => implode(', ', $missing_routes)]));
    }
  }

  /**
   * Gets the routes as an array from form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The list of routes in the form.
   */
  private static function getRoutes(FormStateInterface $form_state): array {
    return array_filter(array_unique(array_map('trim', explode("\n", $form_state->getValue('routes')))));
  }

}
