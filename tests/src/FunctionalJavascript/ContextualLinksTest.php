<?php

declare(strict_types=1);

namespace Drupal\Tests\admin_ui_only\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests contextual links are rendered with Admin UI Only enabled.
 *
 * @group admin_only
 */
class ContextualLinksTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['admin_ui_only', 'block', 'contextual'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->drupalCreateUser(['access contextual links']));
    $this->placeBlock('system_branding_block', [
      'id' => 'branding',
    ]);
  }

  /**
   * Tests the visibility of contextual links.
   */
  public function testContextualLinksVisibility(): void {
    $this->drupalGet('user');
    $contextualLinks = $this->assertSession()->waitForElement('css', '.contextual button', 2000);
    $this->assertEmpty($contextualLinks);

    // Ensure visibility remains correct after cached paged load.
    $this->drupalGet('user');
    $contextualLinks = $this->assertSession()->waitForElement('css', '.contextual button', 2000);
    $this->assertEmpty($contextualLinks);

    // Grant permissions to use contextual links on blocks.
    Role::load(Role::AUTHENTICATED_ID)?->grantPermission('administer blocks')->save();

    $this->drupalGet('user');
    $contextualLinks = $this->assertSession()->waitForElement('css', '.contextual button');
    $this->assertNotEmpty($contextualLinks);

    // Confirm touchevents detection is loaded with Contextual Links
    $this->assertSession()->elementExists('css', 'html.no-touchevents');

    // Ensure visibility remains correct after cached paged load.
    $this->drupalGet('user');
    $contextualLinks = $this->assertSession()->waitForElement('css', '.contextual button');
    $this->assertNotEmpty($contextualLinks);
  }

}
