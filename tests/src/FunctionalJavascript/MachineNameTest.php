<?php

declare(strict_types=1);

namespace Drupal\Tests\admin_ui_only\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests machine name transliteration works with Admin UI Only enabled.
 *
 * @group admin_only
 */
class MachineNameTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['admin_ui_only', 'form_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Make the 'form_test.machine_name' route an admin route so we can access
    // it.
    $this->config('admin_ui_only.settings')->set('routes', ['form_test.machine_name'])->save();
    \Drupal::service('router.builder')->rebuildIfNeeded();
    $account = $this->drupalCreateUser([
      'access content',
    ]);
    $this->drupalLogin($account);
  }

  /**
   * Tests that machine name field functions.
   */
  public function testMachineName(): void {
    // Visit the machine name test page which contains two machine name fields.
    $this->drupalGet('form-test/machine-name');

    $is_drupal_9 = floatval(\Drupal::VERSION) < 10;
    // Test values for conversion.
    $test_values = [
      [
        'input' => 'Test value !0-9@',
        'message' => 'A title that should be transliterated must be equal to the php generated machine name',
        'expected' => $is_drupal_9 ? 'test_value_0_9_' : 'test_value_0_9',
      ],
      [
        'input' => 'Test value',
        'message' => 'A title that should not be transliterated must be equal to the php generated machine name',
        'expected' => 'test_value',
      ],
    ];

    // Get elements from the page.
    $title_1 = $this->assertSession()->fieldExists('machine_name_1_label');
    $machine_name_1_value = $this->assertSession()->elementExists('css', '#edit-machine-name-1-label-machine-name-suffix .machine-name-value');
    $machine_name_2_value = $this->assertSession()->elementExists('css', '#edit-machine-name-2-label-machine-name-suffix .machine-name-value');

    // Test each value for conversion to a machine name.
    foreach ($test_values as $test_info) {
      // Set the value for the field, triggering the machine name update.
      $title_1->setValue($test_info['input']);

      // Wait the set timeout for fetching the machine name.
      $this->assertJsCondition('jQuery("#edit-machine-name-1-label-machine-name-suffix .machine-name-value").html() == "' . $test_info['expected'] . '"');

      // Validate the generated machine name.
      $this->assertEquals($test_info['expected'], $machine_name_1_value->getHtml(), $test_info['message']);

      // Validate the second machine name field is empty.
      $this->assertEmpty($machine_name_2_value->getHtml(), 'The second machine name field should still be empty');
    }
  }

}
