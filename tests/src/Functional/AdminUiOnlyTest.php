<?php

declare(strict_types=1);

namespace Drupal\Tests\admin_ui_only\Functional;

use Drupal\Core\Url;
use Drupal\Tests\admin_ui_only\Traits\ApiRequestTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Admin UI Only module.
 *
 * @group admin_ui_only
 */
class AdminUiOnlyTest extends BrowserTestBase {
  use ApiRequestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'admin_ui_only', 'page_cache', 'test_page_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createContentType(['type' => 'article']);
  }

  /**
   * Tests the normal operation of the module.
   */
  public function testAdminUiOnlyHappyPath(): void {
    $admin_user = $this->createUser([], 'site admin', TRUE);
    $this->drupalLogin($admin_user);

    // Save a node and ensure we are redirected to admin/content.
    $this->drupalGet('node/add/article');
    $this->assertSession()->fieldExists('title[0][value]')->setValue('Test node');
    $this->submitForm([], 'Save');
    $this->assertSession()->addressEquals('admin/content');

    // Ensure accessing the node via HTML results in a 403.
    $node_url = Url::fromUserInput('/node/1');
    $this->drupalGet($node_url);
    $this->assertSession()->statusCodeEquals(403);

    // POST requests to non admin HTML routes should be rejected too.
    $response = $this->makeApiRequest('POST', $node_url, []);
    $this->assertSame(403, $response->getStatusCode());

    $this->drupalGet('user');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('user/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalLogout();

    // 403 for anonymous users too.
    $this->drupalGet($node_url);
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'MISS');

    // Ensure the 403s are cacheable.
    $this->drupalGet($node_url);
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'HIT');

    // Ensure the module can be uninstalled without everything breaking.
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/modules/uninstall');
    $this->assertSession()->fieldExists('uninstall[admin_ui_only]')->check();
    $this->submitForm([], 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldNotExists('uninstall[admin_ui_only]');
    $this->drupalLogout();

    $this->drupalGet($node_url);
    $this->assertSession()->statusCodeEquals(200);

    // Install the module again to ensure it can be.
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/modules');
    $this->assertSession()->fieldExists('modules[admin_ui_only][enable]')->check();
    $this->submitForm([], 'Install');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextMatches('/Module Admin UI Only has been (?:enabled|installed)./');

    $this->drupalGet($node_url);
    $this->assertSession()->statusCodeEquals(403);

    // Ensure redirects work as expected.
    $this->drupalGet('/.well-known/change-password');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('user/2/edit');
  }

  /**
   * Tests that a user can add their own routes.
   */
  public function testSettingsForm(): void {
    // Save a node.
    $admin_user = $this->createUser([], 'site admin', TRUE);
    $this->drupalLogin($admin_user);
    $this->drupalGet('node/add/article');
    $this->assertSession()->fieldExists('title[0][value]')->setValue('Test node');
    $this->submitForm([], 'Save');
    $this->drupalLogout();

    $this->drupalGet('test-page');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('test-render-title');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('node/1');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('node/1');

    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/admin_ui_only');
    $this->assertSession()->fieldExists('error_code')->setValue('404');
    $this->assertSession()->buttonExists('Save configuration')->press();
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->drupalLogout();

    $this->drupalGet('test-page');
    $this->assertSession()->statusCodeEquals(404);
    $this->drupalGet('test-render-title');
    $this->assertSession()->statusCodeEquals(404);
    $this->drupalGet('node/1');
    $this->assertSession()->statusCodeEquals(404);

    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/admin_ui_only');
    $this->assertSession()->fieldExists('routes')->setValue('does_not_exist');
    $this->assertSession()->buttonExists('Save configuration')->press();
    $this->assertSession()->pageTextContains('The following route(s) do not exist: does_not_exist');
    $this->assertSession()->fieldExists('routes')->setValue("test_page_test.test_page\r\ntest_page_test.render_title");
    $this->assertSession()->buttonExists('Save configuration')->press();
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->drupalLogout();

    $this->drupalGet('test-page');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('test-render-title');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('node/1');
    $this->assertSession()->statusCodeEquals(404);
  }

}
