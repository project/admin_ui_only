## Introduction
Limits a Drupal site to only providing an administration UI via HTML.

Install this module to prevent users from accessing content via HTML but allow
access via other request methods. This is useful if you want to allow anonymous
access to a JSON:API or GraphQL endpoint but do not want to allow users to
access content via Drupal.

## Installation
* If the node module is installed, node configuration will be updated to ensure
  you can edit content in the admin UI.
* The frontpage is always accessible via HTML. The best user experience can be
  achieved by setting the frontpage to 'user/login'
* Set your administration theme to be your site's only theme to simplify
  configuration management.
